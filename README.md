# img2css

Bash cli image 2 css converter.

## Dependencies

- mediainfo
- awk
- sed
- grep
- bash

On Ubuntu or debian a simple

`sudo apt-get install mediainfo`

should be sufficient.
Also make sure to make the script executeable:

`sudo chmod +x img2css`

## Using the script

`./img2css <image_file_name>`

It should generate a .html and .css file named after the image.
